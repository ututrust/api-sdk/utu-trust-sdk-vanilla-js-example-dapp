

import {
    EthereumClient,
    w3mConnectors,
    w3mProvider,
    WagmiCore,
    WagmiCoreChains,
    WagmiCoreConnectors,
  } from "https://unpkg.com/@web3modal/ethereum@2.6.2";

  import { Web3Modal } from "https://unpkg.com/@web3modal/html@2.6.2";

  import "https://unpkg.com/@ututrust/web-components@2.0.14/dist/index.js";

  import { ethers } from "https://cdnjs.cloudflare.com/ajax/libs/ethers/6.7.0/ethers.min.js";


  const app = document.getElementById('app');

  let provider;

  if(window.ethereum){
    provider = new ethers.BrowserProvider(window.ethereum);
  }

  const envConfig = {
    apiUrl: 'https://stage-api.ututrust.com'
  }

  console.log(ethers)
  
  
  const initEntity = async (data, product) => {
    const prod = product || {
      ...product,
      ids: {
        // see https://docs.ethers.org/v6/migrating/#migrate-utils
        address: ethers
          .id(product.id)
          .slice(0, 40 + 2)
          .toLowerCase(),
        id: product.id,
      },
      image:
        "https://i0.wp.com/utu.io/wp-content/uploads/job-manager-uploads/company_logo/2020/12/cropped-UTU-LG-FV.png?fit=192%2C192&ssl=1",
    }
    try {
      const response = await fetch(envConfig.apiUrl + "/core-api-v2/entity", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          authorization: `Bearer ${data.access_token}`,
        },
        body: JSON.stringify({
          ...prod
        }),
      });
  
      if (!response.ok) {
        throw new Error("Failed to init entity on utu browser extension");
      }
  
      return response;
    } catch (error) {
      console.log(error);
    }
  };
  
let OFFERS = [
    {
      name: "EVME Social Token",
      id: "product-1",
    },
  {
      name: "DeFi Alliance",
      id: "product-2", 
  },
  {
      name: "MEMEME TKN ",
      id: "product-3"
  }
];

OFFERS = OFFERS.map((offer) => {
  const addr = ethers.id(offer.id).slice(0, 40 + 2).toLowerCase()
  return { 
    name: offer.name,
    id: ethers
    .id(offer.id)
    .slice(0, 40 + 2)
    .toLowerCase(),
    type: "product",
    ids: {
      address: addr,
      id: addr,
    },
    image: "https://i0.wp.com/utu.io/wp-content/uploads/job-manager-uploads/company_logo/2020/12/cropped-UTU-LG-FV.png?fit=192%2C192&ssl=1",

  }
})




const checkMetamaskAndConnect = async () => {
  // Check if MetaMask is installed and connect to it if available
  if (typeof window.ethereum !== 'undefined') {
    console.log('MetaMask is installed!');
    // Connect to MetaMask
    try {
      // Request access to MetaMask accounts
      await window.ethereum.request({ method: 'eth_requestAccounts' });
      return true;
      console.log('Connected to MetaMask');
    } catch (error) {

      if (err.code === 4001) {
        // EIP-1193 userRejectedRequest error
        // If this happens, the user rejected the connection request.
        console.log('Please connect to MetaMask.');
        return false
      }
      console.error('Error connecting to MetaMask:', error);
      return false;
    }
  } else {
    console.log('MetaMask is not installed');
    false;
  }
}


const handleLoadFeedbackDetails = async (sourceEthAddress, authDataResponse) => {
  // The initEntity call is necessary to map the offers in a remote neo4j db
  for (let i = 0; i < OFFERS.length; i++) {
    await initEntity(authDataResponse, OFFERS[i]);
  }
  var button = document.getElementById("connect");
  button.style.display = "none"; 
  // feedback-container
  var feedbackContainer = document.getElementById("feedback-container")
  const root = document.createElement("x-utu-root");
  root.setAttribute("source-uuid", sourceEthAddress);
  root.setAttribute("target-type", "product");
  const ul = document.createElement("ul");
  ul.setAttribute("id", "app");
  

  ul.innerHTML = OFFERS.map(function (offer) {
    return (`<li class="offer">
        "${offer.name}" 
        <x-utu-recommendation id="recommendation" target-uuid="${offer.id}" style="marginTop: -20px;"></x-utu-recommendation> 
        <x-utu-feedback-form-popup source-uuid="${sourceEthAddress}" target-uuid="${offer.id}" transaction-id="${offer.id}"></x-utu-feedback-form-popup>
        <br/><br/><br/><br/>
        <x-utu-feedback-details-popup  source-uuid="${sourceEthAddress}" target-uuid="${offer.id}"></x-utu-feedback-details-popup>
        </li>`);
  }).join('');



  root.appendChild(ul);
  feedbackContainer.appendChild(root)
  
}


const triggerUtuIdentityDataSDKEvent = async (
    identityData
  ) => {
    const event = new CustomEvent("utuIdentityDataReady", {
      detail: identityData,
    });
    window.dispatchEvent(event);

    if(provider && checkMetamaskAndConnect()) {
      // const addr = ethereumClient.getAccount().address
        const signer = await provider.getSigner();
        console.log('signer ', signer)
        const userAddress = await signer.getAddress()
        console.log('User address:', userAddress);
      if(userAddress) {
        handleLoadFeedbackDetails(userAddress.toLowerCase(), identityData)
      }
    }
  };




  document.addEventListener('DOMContentLoaded', () => {

    console.log('app dapp html el', app)

  });

  
  const onConnectToUtuClick = async () => {
    // This passes the wallet provider to the SDK so it can do its magic
    // It effectively logs into the UTU Trust Network services and you get a response object back
    // which encapsulates the successful log in.  Among other things it contains the JWT Token.
    await checkMetamaskAndConnect()
    const addressSignatureVerification = window.utuSdk.addressSignatureVerification
    let authDataResponse = await addressSignatureVerification();
    console.log(authDataResponse);
  
    // this passes the JWT token info to all parts of the SDK. Expect this SDK method to be 
    // refactored into the SDK addressSignatureVerification in later versions of the SDK.
    triggerUtuIdentityDataSDKEvent(authDataResponse);

  
  }


document.getElementById("connect").addEventListener("click", onConnectToUtuClick);




